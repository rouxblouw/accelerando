package logger

import (
	"log"
	"os"
)

// Info prints an info log to stdout
func Info(f string, i ...interface{}) {
	log.Printf("INFO "+f, i...)
}

// Warn prints a warn log to stdout
func Warn(f string, i ...interface{}) {
	log.Printf("WARN "+f, i...)
}

// Error prints an error log to stdout
func Error(f string, i ...interface{}) {
	log.Printf("ERROR "+f, i...)
}

// FatalError prints an error log to stdout and executes os.Exit(1)
func FatalError(f string, i ...interface{}) {
	log.Printf("FATAL "+f, i...)
	os.Exit(1)
}
