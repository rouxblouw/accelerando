package eventing

// Repository is the interface for that all storage implementations
// for accelerando must adhere to
type Repository interface {
	// HealthCheck is a function that tests the functioning of the Repository.
	// Usually a connection test followed by a simple read operation.
	HealthCheck() bool

	// Count should return the size of the event log, or how many events are persisted in this storage.
	Count() int

	// FindByID should return the event specified by the given ID, or return an err.
	FindByID(id string) (Event, error)

	Save(e Event) error
}
