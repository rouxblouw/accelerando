package eventing

import "time"

// Event is the data structure of an event
type Event struct {
	ID        string
	Timestamp time.Time
	EventType EventType
	EventData EventData
}

// TypeName returns the type's string name
func (e Event) TypeName() string {
	return e.EventType.Type()
}

// Data returns the event data interface
func (e Event) Data() interface{} {
	return e.EventData
}

// EventType is the interface for an event type
type EventType interface {
	Type() string
}

// EventData is the interface for event data
type EventData interface{}
