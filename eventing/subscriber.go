package eventing

// Subscriber represents the interface for an event subscriber/listener that can receive and pass events to their handlers
type Subscriber interface {
	Notify(e Event)
}

type subscriberofTypes struct {
	Sub   Subscriber
	Types []EventType
}
