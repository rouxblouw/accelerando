package eventing

import (
	"sync"
	"time"

	"github.com/google/uuid"
)

// NewEventStore returns a new instance of the event store.
// This is the preffered way to start using eventing. Keep one instance of this in your application.
func NewEventStore(r Repository) EventStore {
	return EventStore{
		repo: r,
		subs: &[]subscriberofTypes{},
		lock: sync.RWMutex{},
	}
}

// EventStore is the eventing object to interact with
type EventStore struct {
	repo Repository
	subs *[]subscriberofTypes
	lock sync.RWMutex
}

// IsReady returns wether the store can accept and process events
func (e *EventStore) IsReady() bool {
	return e.repo.HealthCheck()
}

// EventFrom creates and registers a new event from the given input.
// This event is persisted and any subscribers are notified of it's existence.
// Thread safe operations to publish and retry subscriber notifications.
func (e *EventStore) EventFrom(t EventType, d EventData) (Event, error) {
	uuid := uuid.New()
	event := Event{uuid.String(), time.Now(), t, d}
	err := e.repo.Save(event)
	go e.notifySubs(event)
	return event, err
}

// Subscribe adds a subscriber to the eventstore's subscriber list in a thread safe manner.
func (e *EventStore) Subscribe(s Subscriber, types ...EventType) {
	e.lock.Lock()
	*e.subs = append(*e.subs, subscriberofTypes{s, types})
	e.lock.Unlock()
}

// SubscriberCount returns the size of the subscribers list
func (e *EventStore) SubscriberCount() int {
	return len(*e.subs)
}

func (e *EventStore) notifySubs(ev Event) {
	for _, st := range *e.subs {
		for _, a := range st.Types {
			if a.Type() == ev.EventType.Type() {
				st.Sub.Notify(ev)
				break
			}
		}
	}
}
