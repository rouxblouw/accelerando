package tests

import (
	"sync"
	"testing"
	"time"

	"gitlab.com/rouxblouw/accelerando/eventing"
	"gitlab.com/rouxblouw/accelerando/storage"
)

const TYPE1 = TestEventType("Event1")
const TYPE2 = TestEventType("Event2")

func TestInstationOfAccelerandoEventStoreWithDefaultInMemRepo(t *testing.T) {
	eventRepo := storage.NewInMemoryRepository()
	eventStore := eventing.NewEventStore(&eventRepo)
	if !eventStore.IsReady() {
		t.Fatalf("Eventstore health check failed")
	}
}

func TestCreationOfEvent(t *testing.T) {
	eventRepo := storage.NewInMemoryRepository()
	eventStore := eventing.NewEventStore(&eventRepo)
	event, _ := eventStore.EventFrom(TYPE1, TestEventData{"hello"})
	if event.TypeName() != TYPE1.Type() {
		t.Fatalf("Incorrect event type %s", event.TypeName())
	}
	if event.Data().(TestEventData).msg != "hello" {
		t.Fatalf("Incorrect event data %v", event.Data())
	}
}

func TestPersistenceOfEvent(t *testing.T) {
	eventRepo := storage.NewInMemoryRepository()
	eventStore := eventing.NewEventStore(&eventRepo)
	event, _ := eventStore.EventFrom(TYPE1, TestEventData{"hello"})

	if eventRepo.Count() != 1 {
		t.Fatalf("Incorrect amount of events persisted %v", eventRepo.Count())
	}
	e, err := eventRepo.FindByID(event.ID)
	if err != nil || e.EventType != TYPE1 {
		t.Fatalf("Incorrect event persisted %v", e)
	}
}

func TestSubscribtionShouldBeThreadSafe(t *testing.T) {
	eventRepo := storage.NewInMemoryRepository()
	eventStore := eventing.NewEventStore(&eventRepo)
	sub := TestSubscriber(0)
	var wg sync.WaitGroup
	for i := 1; i <= 500; i++ {
		wg.Add(1)
		go func(wg *sync.WaitGroup) {
			defer wg.Done()
			eventStore.Subscribe(&sub, TYPE1)
		}(&wg)
	}
	wg.Wait()
	c := eventStore.SubscriberCount()
	if c != 500 {
		t.Fatalf("Subscribers not added thread safe. %v", c)
	}
}

func TestCreationOfSubscribingOfEventSubscriber(t *testing.T) {
	eventRepo := storage.NewInMemoryRepository()
	eventStore := eventing.NewEventStore(&eventRepo)
	sub := TestSubscriber(0)
	eventStore.Subscribe(&sub, TYPE1)
	subs := eventStore.SubscriberCount()
	if subs != 1 {
		t.Fatalf("Incorrect setting of subs on eventStore")
	}
	eventStore.EventFrom(TYPE1, TestEventData{"hllo"})
	eventStore.EventFrom(TYPE2, TestEventData{"hllo"})
	time.Sleep(100)
	if sub.EventsSeen() != 1 {
		t.Fatalf("Subs returning incorrect events seen with %v", sub.EventsSeen())
	}
}

type TestEventType string

func (t TestEventType) Type() string {
	return string(t)
}

type TestEventData struct {
	msg string
}

func (t TestEventData) Data() interface{} {
	return t
}

type TestSubscriber int

func (t *TestSubscriber) Notify(e eventing.Event) {
	*t = *t + 1
}

func (t *TestSubscriber) EventsSeen() int {
	return int(*t)
}
