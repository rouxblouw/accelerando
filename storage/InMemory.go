package storage

import (
	"errors"

	"gitlab.com/rouxblouw/accelerando/eventing"
)

// NewInMemoryRepository returns an instance of a in memory repo
func NewInMemoryRepository() InMemoryRepository {
	return InMemoryRepository{}
}

// InMemoryRepository is a repository implementation meant for testing
// or demo purposes. NOT RECOMMENDED FOR ANY REAL ENVIRONMENT.
type InMemoryRepository struct {
	events []eventing.Event
}

// HealthCheck implementation of Repository
func (i *InMemoryRepository) HealthCheck() bool {
	return true
}

// Count should return the size of the event log, or how many events are persisted in this storage.
func (i *InMemoryRepository) Count() int {
	return len(i.events)
}

// FindByID should return the event specified by the given ID, or return an err.
func (i *InMemoryRepository) FindByID(id string) (eventing.Event, error) {
	for _, e := range i.events {
		if e.ID == id {
			return e, nil
		}
	}
	return eventing.Event{}, errors.New("Not found")
}

// Save should save the given event to storage
func (i *InMemoryRepository) Save(e eventing.Event) error {
	i.events = append(i.events, e)
	return nil
}
