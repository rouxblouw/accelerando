package web

import (
	"net/http"

	"gitlab.com/rouxblouw/accelerando/web/controller"
)

var routes = []route{
	{
		path:    "/api/health",
		method:  http.MethodGet,
		handler: controller.Health,
	},
}

type route struct {
	path    string
	method  string
	handler func(http.ResponseWriter, *http.Request)
}
