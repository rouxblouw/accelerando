package controller

import (
	"fmt"
	"net/http"
)

// Health returns the health response for the service
func Health(rw http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(rw, "Hello there ;)")
}
