package web

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"gitlab.com/rouxblouw/accelerando/util/logger"

	"github.com/gorilla/mux"
)

// Webserver is the structure for an instance of a webserver
type Webserver struct {
	Port int
}

// Start starts listening on the port 8081 of a seperate goroutine
func (w *Webserver) Start() {
	go w.run()
	logger.Info("Webserver listening on port %d", w.Port)
}

func (w *Webserver) run() {
	m := mux.NewRouter()

	for _, r := range routes {
		m.HandleFunc(r.path, r.handler).Methods(r.method)
	}

	srv := &http.Server{
		Handler:      m,
		Addr:         fmt.Sprintf("0.0.0.0:%d", w.Port),
		WriteTimeout: 10 * time.Second,
		ReadTimeout:  10 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}
