package main

import (
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/rouxblouw/accelerando/util/logger"
	"gitlab.com/rouxblouw/accelerando/web"
)

func main() {
	startWebserver()
	handleShutdown()
}

func startWebserver() {
	w := web.Webserver{Port: 8080}
	w.Start()
}

func handleShutdown() {
	c := make(chan os.Signal)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
	<-c
	logger.Info("Shutting down ...")
}
